let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
   'src/css/bootstrap.min.css',
   'src/css/flatly.css',
   'src/css/style.css'
], 'dist/css/styles.min.css')
.scripts([
   'src/js/jquery.js',
   'src/js/popper.min.js',
   'src/js/bootstrap.min.js',
   'src/js/vue.min.js',
   'src/js/index.js'
], 'dist/js/javascript.min.js')
.copyDirectory('src/assets', 'dist/assets');