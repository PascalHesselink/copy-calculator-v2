var app = new Vue({
  el: '#app',
  data: {
    list: [
      {
        size: "A4",
        original_pages: 0,
        amount: 0,
        total_amount: null,
        total_price_each: null
      }
    ],
    price_scheme: {
    "1-10" : { "1-2":14.3, "3-5":13.3, "6-10":12.4, "11-25":12.4, "26-50":11.4, "51-75":9.6, "76-100":7.1, "101+":4.7 },
    "11-25" : { "1-2":11.9, "3-5":11.5, "6-10":11.5, "11-25":9.6, "26-50":7.1, "51-75":4.7, "76-100":4.7, "101+":4.1 },
    "26-50" : { "1-2":10.0, "3-5":5.4, "6-10":4.7, "11-25":4.7, "26-50":4.3, "51-75":3.8, "76-100":3.4, "101+":3.0 },
    "51-100" : { "1-2":7.2, "3-5":4.1, "6-10":4.0, "11-25":3.0, "26-50":3.6, "51-75":3.3, "76-100":3.0, "101+":2.7 },
    "101-250" : { "1-2":3.6, "3-5":3.7, "6-10":3.6, "11-25":3.3, "26-50":3.1, "51-75":2.9, "76-100":2.5, "101+":2.4 },
    "251-500" : { "1-2":3.1, "3-5":3.1, "6-10":3.1, "11-25":2.9, "26-50":2.9, "51-75":2.6, "76-100":2.4, "101+":2.1 },
    "501-750" : { "1-2":2.6, "3-5":2.6, "6-10":2.6, "11-25":2.6, "26-50":2.6, "51-75":2.4, "76-100":2.1, "101+":1.8 },
    "751-1000" : { "1-2":2.5, "3-5":2.4, "6-10":2.4, "11-25":2.4, "26-50":2.3, "51-75":2.1, "76-100":1.8, "101+":1.8 },
    "1001-2500" : { "1-2":2.1, "3-5":2.1, "6-10":2.1, "11-25":2.1, "26-50":2.1, "51-75":1.8, "76-100":1.8, "101+":1.8 },
    "2501-5000" : { "1-2":2.0, "3-5":2.0, "6-10":2.0, "11-25":1.9, "26-50":1.9, "51-75":1.8, "76-100":1.8, "101+":1.8 },
    "5000+" : { "1-2":2.0, "3-5":1.9, "6-10":1.9, "11-25":1.8, "26-50":1.8, "51-75":1.8, "76-100":1.8, "101+":1.7 }
}
  },
  mounted() {
    var listData = localStorage.getItem("list");
    var parsedListData = JSON.parse(listData);

    if(parsedListData)
      this.list = parsedListData;

    console.log("Copy Calculator V2 - Created by Pascal Hesselink");
  },
  watch:{
    list: {
      handler: function () {
        localStorage.setItem("list", JSON.stringify(this.list));
      },
      deep: true
    }
  },
  methods: {
    calculateItemTotalAmount: function (item) {
      item.total_amount = (item.original_pages * item.amount);
      return item.total_amount;
    },
    calculateItemTotalPrice: function (item) {
      var priceRow = this.getOriginalAmountPrices(item.amount);

      for(var originalPrice in priceRow) {
        if(!originalPrice.includes("+")) {
          if(item.original_pages >= parseInt(originalPrice.split('-')[0]) && item.original_pages <= parseInt(originalPrice.split('-')[1])) {
            var size_multiplier = (item.size === "A4") ? 1 : 2;
            item.total_price_each = ((priceRow[originalPrice] / 100) * size_multiplier).toFixed(3);
            return item.total_price_each;
          }
        } else if(item.original_pages >= parseInt(originalPrice.split('-')[0])) {
            var size_multiplier = (item.size === "A4") ? 1 : 2;
            item.total_price_each = ((priceRow[originalPrice] / 100) * size_multiplier).toFixed(3);
          return item.total_price_each;
        }
      }

      return '';
    },
    getOriginalAmountPrices(amount) {
      for(var priceRow in this.price_scheme) {
        if(!priceRow.includes("+")) {
          if(amount >= parseInt(priceRow.split('-')[0]) && amount <= parseInt(priceRow.split('-')[1])) {
            return this.price_scheme[priceRow];
          }
        } else if(amount > parseInt(priceRow.split('-')[0])) {
          return this.price_scheme[priceRow];
        }
      }

    return null;
    },
    hasCopies: function(type) {
      var boolean = false;
      for (i = 0; i < this.list.length; i++) {
        if(this.list[i].size === type && this.list[i].amount != 0 && this.list[i].original_pages != 0)
          boolean = true;
      }
      return boolean;
    },
    totalInfoOfType: function(type) {
      var objects = {
        total_copies: 0,
        total_price: 0,
      };

      for (i = 0; i < this.list.length; i++) {
        if(this.list[i].size === type) {
          objects.total_copies += this.list[i].total_amount;
          objects.total_price += (this.list[i].total_price_each * this.list[i].total_amount);
        }
      }

      objects.total_price = objects.total_price.toFixed(2);
      return objects;
    },
    validateOrignalPages(item) {
      if(item.original_pages < 0)
        item.original_pages = 0;
    },
    validateAmount(item) {
      if(item.amount < 0)
        item.amount = 0;
    },
    addListItem() {
      this.list.push({
        size: "A4",
        original_pages: 0,
        amount: 0,
        total_amount: null,
        total_price_each: null
      });
    },
    resetListItems() {
      this.list = [];
      this.addListItem();
    },
    deleteListItem(item) {
      if(this.list.length <= 1) {
        this.addListItem();
      }

      this.list.splice(this.list.indexOf(item), 1);
    }
  }
})
